require 'net/http'
class VoicesController < ApplicationController
    protect_from_forgery :except => [:create]
    def new
        @voice_image = VoiceImage.new
        render action: :new
    end

    def createVoice
        @image = VoiceImage.new(voice_image_params)

        if @image.save 
            personal = {'status' => '200'}
            render :json => personal
        else
            personal = {'status' => '500'}
            render :json => personal
        end
    end

    def getVoiceImage
        image_path = Hash[:title => params["id"]]
        @voice_images = VoiceImage.where(voice_id: params["id"])
        #@voice_images.each_with_index do |v_image, index|
            
            #v_image["voice_image_" + index.to_s] = v_image.image_url(:thumb)
            #v_image["voice_image_" + index.to_s] = v_image.url
        #end

        render :json => @voice_images.to_json()
    end

    def getFavoriteVoice
        sql = "select voices.* , users.nick_name, users.profile_picture from voices inner join users on voices.user_id = users.id inner join favorites on voices.id = favorites.voice_id where favorites.user_id=#{session["user_id"]} AND favorites.is_valid = true"
        @voice = ActiveRecord::Base.connection.select_all(sql)

        @voice.each do |v|
            v["created_at"] = v["created_at"].in_time_zone("Tokyo")
            voice_images = VoiceImage.where(voice_id: v["id"])
            voice_images.each_with_index do |v_image, index|
                v["voice_image_" + index.to_s] = v_image.image_url(:thumb)
            end
            @reply = Reply.where(voice_id: v["id"])
            @favo = Favorite.where(voice_id: v["id"])
            @favo.each do |r|
                if r["user_id"].to_s == params["user_id"]
                    v["activate_favo"] = true
                else
                    v["activate_favo"] = false
                end
            end
            @reply.each do |r|
               if r["user_id"].to_s == params["user_id"]
                    v["activate_reply"] = true
                else
                    v["activate_reply"] = false
                end
            end
            v["replys"] = @reply.length
            v["favo"] = @favo.length
        end

        render :json => @voice.to_json()
    end

    def getVoice
        # 10mの緯度
        base_r = 0.000092593
        # GPSの情報で半径30mの緯度
        r = base_r * params["range"].to_f / 10 

        latitude = params["latitude"].to_f
        longitude = params["longitude"].to_f

        sql = "select voices.* , users.nick_name, users.profile_picture from voices inner join users on voices.user_id = users.id where (latitude BETWEEN #{latitude - r} AND #{latitude + r}) AND (longitude BETWEEN #{longitude - r} AND #{longitude + r}) limit 20"
        @voice = ActiveRecord::Base.connection.select_all(sql)
        @voice.each do |v|
            v["created_at"] = v["created_at"].in_time_zone("Tokyo")
            voice_images = VoiceImage.where(voice_id: v["id"])
            voice_images.each_with_index do |v_image, index|
                v["voice_image_" + index.to_s] = v_image.image_url(:thumb)
            end
            @reply = Reply.where(voice_id: v["id"])
            @favo = Favorite.where("voice_id = ? AND is_valid = true", v["id"])
            
            @favo.each do |r|
                if r["user_id"].to_s == params["user_id"]
                    v["activate_favo"] = true
                else
                    v["activate_favo"] = false
                end
            end
            @reply.each do |r|
                if r["user_id"].to_s == params["user_id"]
                    v["activate_reply"] = true
                else
                    v["activate_reply"] = false
                end
            end
            v["replys"] = @reply.length
            v["favo"] = @favo.length
        end
        logger.debug @voice.to_json()
        render :json => @voice.to_json()
    end

    def getMyVoice
        @voice = Voice.where(user_id: session["user_id"])

        render :json => @voice.to_json
    end

    def createImage 
        @image = VoiceImage.new(voice_image_params)
        if @image.save
           personal = {'status' => '200'}
           render :json => personal
        else
	  logger.debug("sample")
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def create
        @voice = Voice.new(voice_params)
        #@image = VoiceImage.new(voice_image_params)

        @voice.user_id = session["user_id"]
        #@voice.tag_list = params["tags"]

        url_request = "http://geoapi.heartrails.com/api/json?method=searchByGeoLocation&y=" + params["latitude"] + "&x=" + params["longitude"]
        url = URI.parse(url_request)
        req = Net::HTTP::Get.new(url.to_s)
        res = Net::HTTP.start(url.host, url.port) {|http|
          http.request(req)
        }
        city = JSON.parse(res.body)
        puts city["response"]["location"][0]["city"] + city["response"]["location"][0]["town"] + "付近"
        @voice.from = city["response"]["location"][0]["city"] + city["response"]["location"][0]["town"] + "付近"

        logger.debug("city[response]")
        if @voice.save
           personal = {'status' => '200', 'voice_id' => @voice.id}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def delete 
        voice = Voice.find(params[:id])
        if voice.destroy
            personal = {'status' => '200'}
            render :json => personal
        end
    end

    def getVoiceById
        sql = "select voices.* , users.nick_name, users.profile_picture from voices inner join users on voices.user_id = users.id where voices.id = #{params[:id]}"
        @voice = ActiveRecord::Base.connection.select_all(sql)
        #@voice.tag = @voice.tag_list


        voice_images = VoiceImage.where(voice_id: @voice[0]["id"])
        voice_images.each_with_index do |v_image, index|
            @voice[0]["voice_image_" + index.to_s] = v_image.image_url(:thumb)
        end

        @reply = Reply.where(voice_id: @voice[0]["id"])
        @favo = Favorite.where("voice_id = ? AND is_valid = true", @voice[0]["id"])
        @favo.each do |r|
            if r["user_id"].to_s == params["user_id"]
                @voice[0]["activate_favo"] = true
            else
                @voice[0]["activate_favo"] = false
            end
        end
        @reply.each do |r|
           if r["user_id"].to_s == params["user_id"]
                @voice[0]["activate_reply"] = true
            else
                @voice[0]["activate_reply"] = false
            end

        end
        @voice[0]["replys"] = @reply.length
        @voice[0]["favo"] = @favo.length

        render :json => @voice.to_json 
    end

    def push_notification
        user = User.find(session["user_id"])
        create_push_notification(user.device_token, params["message"], params["voice_id"])

        personal = {'status' => '200'}
        render :json => personal
    end

    private
    def voice_params
        params.permit(:latitude, :longitude, :content)
    end

    def voice_image_params
        params.permit(:image, :voice_id)
    end
end
