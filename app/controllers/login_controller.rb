class LoginController < ApplicationController
    def pendingLogin
        @user = User.where(UUID: params[:uuid]).first
        if @user != nil
           session["user_id"] = @user.id
           personal = {'result' => 'already existed', "status" => @user.status, "user_id" => @user.id, "user_nick_name" => @user.nick_name, "profile_picture" => @user.profile_picture }
           render :json => personal
        else
           personal = {'result' => 'not found'}
           render :json => personal
        end
    end
end
