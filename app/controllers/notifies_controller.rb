class NotifiesController < ApplicationController
    def new
        @notify = Notify.new()
    end

    def create
        @notify = Notify.new(notify_pramas)
        if @notify.save
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def getActiveNotify
        #sql = "select notifies.* , users.nick_name, users.profile_picture from notifies inner join users on notifies.to_user_id = users.id where notifies.to_user_id=#{session["user_id"]} AND notifies.active = true"
        #@voice = ActiveRecord::Base.connection.select_all(sql)

        @notify = Notify.where(active: false).where(to_user_id: session[:user_id])
        render :json => @notify.to_json()
    end

    def updateAvailable
        count = 0
        ids = params["id"].split(",")
        ids.each do |id|
            notify = Notify.where(id: id).first
            notify.active = true
            if notify.save
               personal = {'status' => '200'}
               count += 1
               #render :json => personal
            else
               personal = {'status' => '500'}
               #render :json => personal
            end
        end
        if count == ids.length 
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def getall
        sql = "select notifies.* , users.nick_name, users.profile_picture from notifies inner join users on notifies.from_user_id = users.id where notifies.to_user_id=#{session["user_id"]}"
        @notify = ActiveRecord::Base.connection.select_all(sql)

        @notify.each do |v|
            v["created_at"] = v["created_at"].in_time_zone("Tokyo")
        end
        render :json => @notify.to_json()
    end

    private
    def notify_params
        params.permit(:to_user_id, :from_user_id, :content , :active, :link)
    end
end
