class UsersController < ApplicationController

    def new
        @user = User.new
    end

    def provisionaryRegist
        @user = User.new
        @user.profile_picture = "profile_pic1.jpeg"
        @user.status = "pending"
        @user.nick_name = params["nick_name"]
        @user.UUID = params["UUID"]
        @user.device_token = params[:device_token]
        @user.isValidNotify   = true
        @user.range_of_voice  = 0.000277778
        @user.range_of_time   = 0
        @user.tag_list        = "" 

        logger.debug "log"

        if @user.save
           user = User.where(uuid: params["UUID"]).first
           session["user_id"] = user.id
           render :json => user.to_json()
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end


    def updateInfo
        @user = User.where(uuid: params[:uuid]).first
        @user.device_token = params[:device_token]
        
        if @user.save
            user = User.where(uuid: params["UUID"]).first
            session["user_id"] = user.id
            render :json => user.to_json()
        else
            personal = {'status' => '500'}
            render :json => personal
        end
    end

    def register 
        @user = User.where(uuid: params[:uuid]).first
        @user.sex          = params[:sex]
        @user.facebook_id  = params[:facebook_id]
        @user.last_name    = params[:last_name]
        @user.first_name   = params[:first_name]
        @user.mail_address = params[:mail_address]
        @user.mail_address = params[:mail_address]
        @user.birthday     = params[:birthday]
        @user.status       = "valid"

        if @user.save 
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def updateSetting
        @user = User.find(session["user_id"])
        if @user.update(user_update_setting)
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def create 
        @user = User.new(provisionary_params)
        @user.profile_picture = "profile_pic1.jpeg"
        @user.isValidNotify   = true
        @user.range_of_voice  = 0.000277778
        @user.range_of_time   = 0
        @user.tag_list        = "" 

        if @user.save
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def getUser
        @user = User.find(params[:user_id])

        render :json => @user.to_json()
    end

    private
    def user_params
        params.permit(:nick_name, :sex, :full_name, :age, :mail_address, :address)
    end

    def user_update_setting
        params.permit(:profile_picture, :isValidNotify, :range_of_voice, :range_of_time, :tag_list, :nick_name)
    end

    def provisionary_params
        params.permit(:name, :uuid)
    end
end
