
class FavoritesController < ApplicationController

    def activate
        if favo = isExistFavo?(params["user_id"], params["voice_id"]) 
            favo.is_valid = true
        else
            favo = Favorite.new(favorite_params)
        end
        
        if favo.save
            voice = Voice.where(id: params[:voice_id]).first
            user = User.find(params["user_id"])
            voice_auth_user = User.find(voice.user_id)
            create_notification( voice.user_id, params["user_id"], "あなたのVoiceに#{user.nick_name}がfavoしました", "", params[:voice_id])
            create_favo_push_notification(voice_auth_user.device_token, "あなたのVoiceに#{user.nick_name}がfavoしました", params[:voice_id])
            personal = {'status' => '200'}
            render :json => personal
        else
            personal = {'status' => '500'}
            render :json => personal
        end
    end

    def deactivate
        favo = Favorite.where(voice_id: params["voice_id"]).where(user_id: session["user_id"]).first
        favo.is_valid = false

        if favo.save
           personal = {'status' => '200'}
           render :json => personal
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    private
    def favorite_params
        params.permit(:voice_id, :user_id, :is_valid)
    end

    def favorite_update
        params.permit(:is_valid)
    end

    def isExist?
        favo = Favorite.where(user_id: params["user_id"]).where(voice_id: params["voice_id"])
        if favo
            return true
        else
            return false
        end
    end
end

