class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def create_notification(to_user_id, from_user_id, message, from, voice_id )
    logger.debug "enter"
    @notify = Notify.new()
    @notify.to_user_id = to_user_id
    @notify.from_user_id = from_user_id
    @notify.content = message
    @notify.link = voice_id
    @notify.active = false

    if @notify.save
        logger.debug "success"
        return "success"        
    else
        logger.debug "failed"
        return "faliled"
    end
  end

  def create_push_notification(token, message, voice_id)
    APNS.host = 'gateway.sandbox.push.apple.com' 
    APNS.pem = "#{Rails.root}/app/Certificates_developments.pem"
    APNS.port = 2195
    device_token = token
    #APNS.send_notification(device_token, :alert => message, :badge => 1, :sound => 'default')
    APNS.send_notification(device_token, :alert => message, :badge => 1, :sound => 'default', :other => { :aps => {:category => 'custom', :kind => 'voice' , :alert => message, :sound => 'default', :badge => 1, :voice_id => voice_id} })
  end

  def create_reply_push_notification(token, message, voice_id)
        APNS.host = 'gateway.sandbox.push.apple.com' 
        APNS.pem = "#{Rails.root}/app/Certificates_developments.pem"
        APNS.port = 2195
        device_token = token
        APNS.send_notification(device_token, :alert => message, :badge => 1, :sound => 'default', :other => { :aps => {:kind => 'reply', :alert => message, :sound => 'default', :badge => 1, :voice_id => voice_id} })
  end


  def create_favo_push_notification(token, message, voice_id)
    APNS.host = 'gateway.sandbox.push.apple.com' 
    APNS.pem = "#{Rails.root}/app/Certificates_developments.pem"
    APNS.port = 2195
    device_token = token
    APNS.send_notification(device_token, :alert => message, :badge => 1, :sound => 'default', :other => { :aps => {:kind => 'favo', :alert => message, :sound => 'default', :badge => 1, :voice_id => voice_id} })
  end

  def create_notify_notification(token, message) 
    APNS.host = 'gateway.sandbox.push.apple.com' 
    APNS.pem = "#{Rails.root}/app/Certificates_developments.pem"
    APNS.port = 2195
    device_token = token
    APNS.send_notification(device_token, :alert => message, :badge => 1, :sound => 'default')
  end

  def isExistFavo?(user_id, voice_id)
     favorite = Favorite.where(user_id: user_id).where(voice_id: voice_id).first

     if favorite != nil
         return favorite 
     else
         return false
     end
 
  end

  def sign_in?
    if session["user_id"] != nil
        return true
    else
        return false
    end
  end

end
