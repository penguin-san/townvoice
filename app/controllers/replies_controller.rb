class RepliesController < ApplicationController
    def new
        @reply = Reply.new()
    end

    def create
        @reply = Reply.new(reply_params)
        if @reply.save
           user = User.find(session["user_id"])
           voice_auth = Voice.find(params[:voice_id])
           voice_auth_user = User.find(voice_auth.user_id)

           if voice_auth.user_id != user.id
               create_notification( voice_auth.user_id, user.id, "あなたのvoiceに#{user.nick_name}さんがコメントされました", user.nick_name, params[:voice_id])
               create_reply_push_notification(voice_auth_user.device_token, "あなたのvoiceに#{user.nick_name}さんがコメントされました", voice_auth.id)
           end

            sql = "select replies.content, replies.created_at, users.id, users.nick_name, users.device_token from replies left outer join users on replies.user_id = users.id where replies.voice_id = #{params[:voice_id]} "
            @reply = ActiveRecord::Base.connection.select_all(sql)
            @reply.each do |v|
                if v["id"] != session["user_id"]
                    create_notification( v["id"], user.id, "あなたがコメントしたvoiceに#{user.nick_name}さんもコメントされました", user.nick_name, params[:voice_id])
                    create_reply_push_notification(v["device_token"], "他の人もあなたがコメントしたvoiceにコメントしています", v["id"])
                end
            end

           if favo = isExistFavo?(session["user_id"], params["voice_id"]) 
               favo.is_valid = true
           else
               favo = Favorite.new
               favo.user_id = session["user_id"]
               favo.voice_id = params[:voice_id]
               favo.is_valid = true
           end
            
           if favo.save
               personal = {'status' => '200'}
               render :json => personal
           else
               personal = {'status' => '500'}
               render :json => personal
           end
        else
           personal = {'status' => '500'}
           render :json => personal
        end
    end

    def getByVoiceid
        sql = "select replies.id, replies.content, replies.created_at, replies.user_id, users.nick_name, users.profile_picture from replies left outer join users on replies.user_id = users.id where replies.voice_id = #{params[:id]} "
        @reply = ActiveRecord::Base.connection.select_all(sql)
        @reply.each do |v|
            v["created_at"] = v["created_at"].in_time_zone("Tokyo")
        end
        
        render :json => @reply.to_json
    end

    private
    def reply_params
        params.permit(:voice_id, :content , :user_id, :from)
    end
end

