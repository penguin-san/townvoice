class User < ActiveRecord::Base
    has_many :voices
    has_many :replies
    has_many :profile_images
end
