class Voice < ActiveRecord::Base
    belongs_to :user, :foreign_key => 'user_id'
    has_many :voice_images
    acts_as_taggable
end
