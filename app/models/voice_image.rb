class VoiceImage < ActiveRecord::Base
    mount_uploader :image, ImageUploader
    belongs_to :voice
end
