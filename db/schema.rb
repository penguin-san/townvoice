# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150728032106) do

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "voice_id",   limit: 4
    t.boolean  "is_valid",   limit: 1
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "notifies", force: :cascade do |t|
    t.integer  "to_user_id",   limit: 4
    t.integer  "from_user_id", limit: 4
    t.string   "content",      limit: 255
    t.boolean  "active",       limit: 1
    t.string   "link",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "image",      limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "profile_images", force: :cascade do |t|
    t.string   "image",      limit: 255
    t.boolean  "valid",      limit: 1
    t.boolean  "selected",   limit: 1
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "replies", force: :cascade do |t|
    t.integer  "voice_id",   limit: 4
    t.string   "content",    limit: 255
    t.integer  "user_id",    limit: 4
    t.string   "from",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "nick_name",       limit: 255
    t.string   "UUID",            limit: 255
    t.string   "device_token",    limit: 255
    t.string   "facebook_id",     limit: 255
    t.string   "sex",             limit: 255
    t.string   "last_name",       limit: 255
    t.string   "first_name",      limit: 255
    t.string   "age",             limit: 255
    t.string   "mail_address",    limit: 255
    t.string   "current_city",    limit: 255
    t.string   "birthday",        limit: 255
    t.string   "status",          limit: 255
    t.string   "profile_picture", limit: 255
    t.boolean  "isValidNotify",   limit: 1
    t.float    "range_of_voice",  limit: 24
    t.integer  "range_of_time",   limit: 4
    t.string   "tag_list",        limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "voice_images", force: :cascade do |t|
    t.string   "image",      limit: 255
    t.integer  "voice_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "voices", force: :cascade do |t|
    t.decimal  "latitude",               precision: 11, scale: 8
    t.decimal  "longitude",              precision: 11, scale: 8
    t.string   "content",    limit: 255
    t.string   "tag",        limit: 255
    t.integer  "user_id",    limit: 4
    t.string   "from",       limit: 255
    t.boolean  "available",  limit: 1
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

end
