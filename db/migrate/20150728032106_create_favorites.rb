class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.integer :user_id
      t.integer :voice_id
      t.boolean :is_valid

      t.timestamps null: false
    end
  end
end
