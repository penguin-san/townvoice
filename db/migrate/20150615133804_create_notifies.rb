class CreateNotifies < ActiveRecord::Migration
  def change
    create_table :notifies do |t|
      t.integer :to_user_id
      t.integer :from_user_id
      t.string :content
      t.boolean :active
      t.string :link

      t.timestamps null: false
    end
  end
end
