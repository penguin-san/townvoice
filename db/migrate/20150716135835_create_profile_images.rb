class CreateProfileImages < ActiveRecord::Migration
  def change
    create_table :profile_images do |t|
      t.string :image
      t.boolean :valid
      t.boolean :selected
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
