class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.integer :voice_id
      t.string :content
      t.integer :user_id
      t.string :from

      t.timestamps null: false
    end
  end
end
