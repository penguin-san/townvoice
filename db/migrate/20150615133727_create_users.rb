class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nick_name 
      t.string :UUID
      t.string :device_token
      t.string :facebook_id
      t.string :facebook_id
      t.string :sex
      t.string :last_name
      t.string :first_name
      t.string :age
      t.string :mail_address
      t.string :current_city
      t.string :birthday
      t.string :status
      
      # can edit in setting 
      t.string :profile_picture
      t.boolean :isValidNotify
      t.float  :range_of_voice
      t.integer :range_of_time
      t.string :tag_list

      t.timestamps null: false
    end
  end
end
