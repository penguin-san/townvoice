class CreateVoiceImages < ActiveRecord::Migration
  def change
    create_table :voice_images do |t|
      t.string :image
      t.integer :voice_id

      t.timestamps null: false
    end
  end
end
