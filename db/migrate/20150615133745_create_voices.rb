class CreateVoices < ActiveRecord::Migration
  def change
    create_table :voices do |t|
      t.decimal :latitude,:precision => 11, :scale => 8
      t.decimal :longitude,:precision => 11, :scale => 8
      t.string :content
      t.string :tag
      t.integer :user_id
      t.string :from
      t.boolean :available

      t.timestamps null: false
    end
  end
end
