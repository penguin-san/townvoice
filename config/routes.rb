Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  get 'users/create'             => "users#create"
  get "users/provisionaryRegist" => "users#provisionaryRegist"
  get "users/register"           => "users#register"
  get "users/getuser"            => "users#getUser"
  get "users/updateSetting"      => "users#updateSetting"

  get 'voices/new'               => "voices#new"
  get 'voices/create'            => "voices#create"
  post 'voices/createImage'      => "voices#createImage"
  get 'voices/getmyvoices'       => "voices#getMyVoice"
  get 'voices/getvoices'         => "voices#getVoice"
  get "voices/getVoicesbyId"     => "voices#getVoiceById"
  get "voices/notifications"     => "voices#push_notification"
  post "voices/create"           => "voices#create"
  get "voices/getFavoriteVoice"  => "voices#getFavoriteVoice"
  get "voices/delete"            => "voices#delete"

  get "notifies/getActiveNotify" => "notifies#getActiveNotify"
  get "notifies/updateAvailable" => "notifies#updateAvailable"
  get "notifies/getall"          => "notifies#getall"

  get "replies/create"           => "replies#create"
  get "replies/getreplyid"       => "replies#getByVoiceid"

  get "login/pendingLogin"       => "login#pendingLogin"

  get "favorites/activate"       => "favorites#activate"
  get "favorites/deactivate"     => "favorites#deactivate"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
